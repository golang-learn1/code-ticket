package main

import (
	"fmt"
	"math/rand"
)

type ticket struct{
	spaceline, tripType string
	days, price int
}

func (t *ticket) getTicket(){
	t.spaceline = typeOfSpacelines[rand.Intn(len(typeOfSpacelines))]
	t.tripType  = typeOfTrip[rand.Intn(len(typeOfTrip))]
	t.days      = 62100000 / t.getSpeedOfspacelines(t.spaceline) / 24
	t.price 	= 36 + speedOfSpacelines[t.spaceline] * priceMultiplier[t.tripType]
}
func (t ticket) getSpeedOfspacelines(spaceline string) int{
	if speedOfSpacelines[spaceline] != 0 {
		return speedOfSpacelines[spaceline]
	}
	speedOfSpacelines[spaceline] = rand.Intn(14) + 16
	return speedOfSpacelines[spaceline]
}

var speedOfSpacelines map[string]int = make(map[string]int)
var priceMultiplier map[string]int = map[string]int {
	typeOfTrip[0] : 1,
	typeOfTrip[1] : 2,
}
var typeOfSpacelines []string = []string {
	"Space Adventures", "SpaceX", "Galactic",
}
var typeOfTrip []string = []string {
	"One-way",
	"Round-trip",
}

func main(){
	newTicket := ticket{}
	fmt.Printf("%-16v\t%-10v\t%-10v\t%v \n\n", "Spaceline", "Days", "Type", "Price")
	for i := 1; i < 10; i++ {
		newTicket.getTicket()
		fmt.Printf( "%-16v\t%-10v\t%-10v\t%v \n", newTicket.spaceline, newTicket.tripType, newTicket.days, newTicket.price)
	}
}